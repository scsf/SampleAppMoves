import {AppRegistry} from 'react-native';
import {StackNavigator,} from 'react-navigation';

import First from './js/pager/First'
import Second from './js/pager/Second'
import Login from './js/pager/login.android'
import LoginOk from "./js/pager/LoginOk";
import Sign from "./js/pager/sign.android";


const App = StackNavigator({
    First: {screen: First},
    Second: {screen: Second},
    Login: {screen: Login},
    LoginOk: {screen: LoginOk},
    Sign: {screen: Sign},
});

AppRegistry.registerComponent('SampleAppMoves', () => App);

/**
 *
 * Copyright 2015-present reading
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
import {StackNavigator, TabNavigator} from 'react-navigation';
import CategoryContainer from '../containers/CategoryConatiner';
import MainContainer from '../containers/MainConatiner';
import Feedback from '../pages/Feedback/Feedback';
import About from '../pages/About/About';

const TabContainer = TabNavigator(
    {
        Main: {screen: MainContainer},
        Category: {screen: CategoryContainer},
        Feedback: {screen: Feedback},
        About: {screen: About}
    },
    {
        lazy: true,
        tabBarPosition: 'bottom',
        tabBarOptions: {
            activeTintColor: '#e9211d',
            inactiveTintColor: '#0f991e',
            showIcon: true,
            style: {
                backgroundColor: '#fffcf6'
            },
            indicatorStyle: {
                opacity: 0
            },
            tabStyle: {
                padding: 0
            }
        }
    }
);

const App = StackNavigator(
    {
        Home: {
            screen: TabContainer,
            navigationOptions: {
                headerLeft: null,
                title:"你好",
                fontSize:50,
            }
        },

    },
    {
        headerMode: "screen",
        navigationOptions: {
            headerStyle: {
                backgroundColor: "#2a58e9"
            },
            headerTitleStyle: {
                color: "#ff3131",
                fontSize: 20,
            },
            headerTintColor: "#fff8f9"
        }
    }
);

export default App;

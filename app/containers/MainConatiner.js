import React from 'react';


import {Text} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';


export default class MainContainer extends React.Component {
    static navigationOptions = {
        title: '首页',
        tabBarIcon: ({tintColor}) => (
            <Icon name="md-home" size={25} color={tintColor}/>
        )
    };

    render() {
        return (
            <Text>Hello,Main</Text>
        );
    }
}

import React, {Component} from 'react';
import {
    View
} from 'react-native';

import App from './containers/app'

class Root extends Component {

    render() {
        return (
            <View style={{flex: 1}}>
                <App />
            </View>
        );
    }

}

export default Root;

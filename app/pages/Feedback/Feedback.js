import React from 'react';


import {Text} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';

export default class Feedback extends React.Component {
    static navigationOptions = {
        title: '反馈',
        tabBarIcon: ({tintColor}) => (
            <Icon name="md-checkmark" size={25} color={tintColor}/>
        )
    };

    render() {
        return (
            <Text>Hello,Feedback</Text>
        );
    }
}

import React from 'react';


import {Text} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';

export default class About extends React.Component {
    static navigationOptions = {
        title: '关于',
        tabBarIcon: ({tintColor}) => (
            <Icon name="md-information-circle" size={25} color={tintColor}/>
        )
    };

    render() {
        return (
            <Text>Hello,Feedback</Text>
        );
    }
}

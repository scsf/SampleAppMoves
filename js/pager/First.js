import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';

export default class First extends Component {
    static navigationOptions = {
        title: '首页',
        headerStyle: {
            backgroundColor: "#6280e9"
        },
        headerTitleStyle: {
            color: "#f5fff8",
            fontSize: 20,
        },
        headerTintColor: "#fff8f9"
    };

    render() {
        const {navigate} = this.props.navigation;
        return (
            <View style={firstStyle.containerStyle}>
                < Text
                    style={firstStyle.buttonStyle}
                    onPress={() =>
                        navigate('Second', {name: 'Jane'})
                    }>跳转到二个页面</Text>
                < Text
                    style={firstStyle.buttonStyle}
                    title="去登陆"
                    onPress={() =>
                        navigate('Login', {name: 'gy'})
                    }>去登陆</Text>


                <View><Text></Text></View>
                <View><Text></Text></View>
                <View><Text></Text></View>

            </View>
        );
    }
}

const firstStyle = StyleSheet.create({
    containerStyle: {
        flex: 1,
        flexDirection: "column",
        backgroundColor: "#f6fffa",
        alignItems: "center",
    },
    buttonStyle: {
        backgroundColor: "#3d80ff",
        borderRadius: 10,
        height: 45,
        width: 120,
        marginTop: 15,
        textAlign: "center",
        textAlignVertical: "center",
        color: "#f4fff3"
    }
});
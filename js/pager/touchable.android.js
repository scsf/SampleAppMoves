import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Button
} from 'react-native';

import LayoutDemo from '../component/touchable_demo'

export default class main extends Component {

    render() {
        return (
            <View style={{flex: 1}}>
                <LayoutDemo/>
            </View>
        );
    }
}

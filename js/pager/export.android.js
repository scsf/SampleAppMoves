import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Button
} from 'react-native';

import Export, {a, b, sum} from '../component/export_demo_android';

export default class main extends Component {

    render() {
        return (
            <View>
                <Export/>
                <Text>{a}</Text>
                <Text>{b}</Text>
                <Text>{sum(1, 6)}</Text>
            </View>
        );
    }
}
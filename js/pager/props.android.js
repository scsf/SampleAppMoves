import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Button
} from 'react-native';

import Myprops from '../component/props_demo'

export default class main extends Component {

    render() {
        var param = {name: "小白", age: 18}
        return (
            <View>
                <Myprops {...param}/>
            </View>
        );
    }
}
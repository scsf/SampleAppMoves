import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Button
} from 'react-native';

import Myprops from '../component/state_demo'

export default class main extends Component {

    // state = {
    //     mysize: 0
    // }

    constructor(props) {
        super(props);
        this.state = ({
            mysize: 0
        })
    }


    render() {
        return (
            <View>
                <Text onPress={() => {
                    var size = this.refs.refMyProps.getSize();
                    this.setState({
                        mysize: size
                    })
                }}>
                    获取气球大小：{this.state.mysize}
                </Text>
                <Myprops ref="refMyProps"/>
            </View>
        );
    }
}

import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';

export default class First extends Component {

    render() {
        return (
            <View style={firstStyle.containerStyle}>
                < Text>Login Ok</Text>
            </View>
        );
    }
}

const firstStyle = StyleSheet.create({
    containerStyle: {
        flex: 1,
        flexDirection: "column",
        backgroundColor: "#f6fffa",
        alignItems: "center",
    },
    buttonStyle: {
        backgroundColor: "#3d80ff",
        borderRadius: 10,
        height: 45,
        width: 100,
        marginTop: 15,
        textAlign: "center",
        textAlignVertical: "center",
    }
});
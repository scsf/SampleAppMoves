/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View
} from 'react-native';


export default class hello extends Component {
    render() {
        return (
            <View style={styles.containerStyle}>
                <Text style={styles.welcome}>
                    Welcome to React Native!
                </Text>
                <Text onPress={this._onClickMovie()} style={styles.movieConatiner}>点击我跳转到电影列表</Text>
            </View>
        );
    }

    _onClickMovie() {

    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        flex: 1,
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    movieConatiner: {
        marginTop: 20,
        backgroundColor: '#0ff',
        alignContent: 'center',
        height: 35,
        width: 500,
        textAlign: 'center',
    }
});

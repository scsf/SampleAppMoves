import React, {Component} from 'react';
import {View, Text} from 'react-native';
import Hello from '../component/lifecycle_component.android.js';

export default class lifecycle extends Component {

    constructor() {
        super()
        this.state = {
            remove: false
        }
    }

    render() {
        var view = this.state.remove ? null : <Hello/>;
        var text = this.state.remove ? "让他复活" : "打死他";
        return (
            <View style={{flex: 1}}>
                <Text style={{fontSize: 20}}
                      onPress={() => {
                          this.setState({
                              remove: !this.state.remove
                          })
                          ;
                      }}>
                    {text}
                </Text>

                {view}
            </View>
        );
    }
}
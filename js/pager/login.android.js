import React, {Component} from 'react';
import {Alert, Image, StyleSheet, Text, TextInput, View} from 'react-native';


export default class LoginComponent extends Component {

    static navigationOptions = {
        title: null,
        headerStyle: {
            backgroundColor: "#3361e9"
        },
        headerTitleStyle: {
            color: "#f5fff8",
            fontSize: 20,
        },
        headerTintColor: "#fff8f9",
        //隐藏标题栏
        header: null,
    };


    constructor(props) {
        super(props)
        this.state = ({
            user: "default",
            psw: "default",
        })
    }

    goSign() {
        // const {navigate} = this.props.navigation;
        // navigate('Sign', {name: 'Sign'})

        var baseUrl = "https://staging.tophold.com/api/v2/";
        var loginApi = "users/login";

        var loginJson = fetch(baseUrl + loginApi, {
            method: 'POST',
            headers: {
                'Accept': 'application/*',
                'Content-Type': 'application/x-www-form-urlencoded',
                'X-TopHold-AppVersion': '3.0.7',
                'X-TopHold-OS': 'Android',
                'X-TopHold-OSVersion': '4.2',
                'X-TopHold-DeviceId': '123456',
            },
            body: 'login=' + this.state.user + '&password=' + this.state.psw
        }).then(data => data.json())
            .then(data => JSON.stringify(data))
            .then(data => {
                //在这里处理业务逻辑
                var user = JSON.parse(data);
                console.log(user.toString());
                if (user.error == null) {
                    window.user = data;//保存到window中
                    const {navigate} = this.props.navigation;
                    navigate('Sign', {name: 'gy'})
                } else {
                    Alert.alert("用户名或密码错误");
                }
            })
            .catch(error => {
                console.error(JSON.stringify(error))
            });

        console.log(loginJson)
    }

    render() {
        return (
            <View>
                <Image
                    style={loginStyles.topImg}
                    source={require('../../img/login_bg.png')}/>

                <View style={loginStyles.coreConatinerStyle}>

                    <TextInput style={loginStyles.inputStyle}
                               placeholder="手机号/用户名/邮箱"
                               placeholderTextColor={"#b2b2b2"}
                               underlineColorAndroid={"#b2b2b2"}
                               onChangeText={(text) => {
                                   this.setState({
                                       user: text.toLocaleString()
                                   })
                               }}
                    />
                    <TextInput style={loginStyles.inputStyle}
                               placeholder="密码"
                               placeholderTextColor={"#b2b2b2"}
                               underlineColorAndroid={"#b2b2b2"}
                               secureTextEntry={true}
                               onChangeText={(text) => {
                                   this.setState({
                                       psw: text.toLocaleString()
                                   })
                               }}/>

                    <Text
                        style={loginStyles.sumStyle}
                        onPress={() => {

                            Alert.alert("提醒", "用户：" +
                                this.state.user
                                + ",密码：" + this.state.psw
                                + "是否要登录？", [
                                    {
                                        text: "取消", onPress: () => {

                                        },
                                        style: "cancel",
                                    },
                                    {
                                        text: "确认", onPress: () => {
                                            this.goSign();
                                        },
                                        style: "ok",
                                    },

                                ]
                            )
                        }}
                    >登录</Text>
                </View>
            </View>
        );
    }
}

const loginStyles = StyleSheet.create({
    topImg: {
        width: "100%",
        height: 184,
        resizeMode: "stretch"
    },
    coreConatinerStyle: {
        // flex: 1,
        backgroundColor: "#f5f5f5",
        flexDirection: "column",
        // justifyContent: "center",
        paddingLeft: 12,
        paddingRight: 12,
    },
    inputStyle: {},
    sumStyle: {
        height: 44,
        marginTop: 20,
        backgroundColor: "#ec3627",
        color: "#FFFFFF",
        fontSize: 17,
        textAlign: "center",
        textAlignVertical: "center",
        // fontWeight:"bold",
        borderRadius: 22,//设置四角弧度
    }
});
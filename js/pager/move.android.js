import React, {Component} from 'react';
import {
    AppRegistry,
    Image,
    FlatList,
    StyleSheet,
    Text,
    View
} from 'react-native';

/**
 * 为了避免骚扰，我们用了一个样例数据来替代Rotten Tomatoes的API
 * 请求，这个样例数据放在React Native的Github库中。
 * 当然，由于众所周知的原因，这个地址可能国内访问也比较困难。
 */
var REQUEST_URL = 'https://raw.githubusercontent.com/facebook/react-native/0.51-stable/docs/MoviesExample.json';

var MOVES_DATA = [
    {
        title: '标题',
        year: '2015',
        posters: {thumbnail: 'https://pic1.zhimg.com/80/b28a63d8a60fd331df43c91a22a1cc10_hd.jpg'}
    }
];


var styles = StyleSheet.create(
    {
        container: {
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#f6faff',
        },
        rightContainer1: {
            flex: 1,
            height: 150,
            backgroundColor: "#87ffc3",
            alignItems: 'center'
        },
        rightContainer2: {
            flex: 1,
            height: 150,
            backgroundColor: "#ffdd79"
        },
        title: {
            fontSize: 20,
            marginBottom: 8,
            textAlign: 'center',
        },
        year: {
            textAlign: 'center'
        },
        thumbnail: {
            width: 93,
            height: 141,
        },
        list: {
            padding: 20,
            backgroundColor: "#ffeaf6",
        }
    }
);


export default class move extends Component {
    constructor(props) {
        super(props);
        this.state = {
            // movies: null,
            data: [],
            loaded: false,
        };

        this.fetchData = this.fetchData.bind(this);
    }

    componentDidMount() {
        this.fetchData();
    }

    fetchData() {
        fetch(REQUEST_URL)
            .then((response) => response.json())
            .then(responseData => {
                this.setState({
                    // movies: responseData.movies,
                    data: this.state.data.concat(responseData.movies),
                    loaded: true,
                });
            });
    }

    render() {

        if (!this.state.loaded) {
            return this.renerLoadingView();
        }

        // var movie = this.state.movies[0];
        // return this.renderMove(movie);
        return (
            <FlatList
                data={this.state.data}
                renderItem={this.renderMove}
                style={styles.list}
            />
        );
    }

    renerLoadingView() {
        return (
            <View style={styles.containerStyle}>
                <Text>
                    正在加载中...
                </Text>
            </View>
        );
    }

    renderMove(movies) {
        var item = movies.item;
        return (
            <View style={styles.containerStyle}>
                <Image style={styles.thumbnail}
                       source={{uri: item.posters.thumbnail}}
                />
                <View style={(movies.index % 2 == 0
                    ? styles.rightContainer1
                    : styles.rightContainer2)}>
                    < Text style={styles.title}> {item.title}</Text>
                    <Text style={styles.year}>{item.year}</Text>
                </View>
            </View>
        );
    }
}
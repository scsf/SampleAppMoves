import React, {Component} from 'react';
import {Alert, FlatList, Image, StyleSheet, Text, TouchableHighlight, View} from 'react-native';

export default class sign extends Component {

    static navigationOptions = {
        // title: '签到',
        headerTitle: "签到",
        headerStyle: {
            backgroundColor: "#ec3627",
        },
        headerTitleStyle: {
            color: "#ffffff",
            fontSize: 20,
            //居中显示
            alignSelf: 'center',
        },
        headerTintColor: "#f0f1ff",
        headerLeft: (

            <TouchableHighlight
                underlayColor={"#ec3627"}

                onPress={() => {
                    Alert.alert("退出");
                }}>

                <Image
                    source={require('../../img/back.png')}
                    style={{
                        marginLeft: 10,
                        width: 25,
                        height: 25,
                    }}

                />
            </TouchableHighlight>

        ),
        headerRight: (
            <Text style={{
                textAlign: "center",
                textAlignVertical: "center",
                color: "#fff",
                fontSize: 14,
                marginRight: 10,
            }}
                  onPress={() => {
                      Alert.alert("积分规则");
                  }}

            >积分规则</Text>
        ),
        gesturesEnabled: true,
    };

    constructor(props) {
        super(props)
        this.state = ({
            user: "default",
            psw: "default",
        })
    }

    //  state = {
    //     user: "",
    //     psw: "",
    // }
    _separator = () => {
        return <View style={{height: 0.5, backgroundColor: '#999999'}}/>;
    }
    _renderItem = (item) => {
        var txt = item.item.key + item.index;
        var bgColor = item.index % 2 == 0 ? '#dafffa' : '#faffe7';
        return <Text
            style={[{flex: 1, height: 100, backgroundColor: bgColor},
                signStyles.itemStyle]}>{txt}</Text>
    }

    refreshing() {
        let timer = setTimeout(() => {
            clearTimeout(timer)
            alert('刷新成功')
        }, 1500)
    }

    render() {
        var user = window.user;
        var userObj = null;
        var headUrl = null;
        if (user != null) {
            userObj = JSON.parse(user);
            console.log("---" + userObj.user.avatar_url);
            headUrl = userObj.user.avatar_url;
        } else {
            headUrl = '../../img/ico_default_user_avatar.png';
        }

        return (
            <View style={{backgroundColor: "#f5f5f5"}}>
                {/*上面一块*/}
                <View style={signStyles.topConatinerStyle}>
                    {/*头像、签到按钮等*/}
                    <View
                        style={signStyles.topTopConatinerStyle}>
                        <Image style={signStyles.headImgStyle}
                               source={{uri: headUrl}}/>

                        <View style={signStyles.integralConatinerStyle}>
                            <Text style={signStyles.integralStyle}>
                                0积分
                            </Text>
                            <Text style={signStyles.integralHintStyles}>
                                连签一周，周五一周积分翻倍
                            </Text>
                        </View>

                        {/*签到按钮*/}
                        <Text style={signStyles.signTxtStyle}>
                            今日签到+10
                        </Text>

                    </View>

                    {/*兑换记录、积分记录*/}
                    <View style={signStyles.topBottomConatinerStyle}>

                        <View style={signStyles.topBottomTxtConatinerStyle}>
                            <Image style={{width: 16, height: 16, resizeMode: "center"}}
                                   source={require('../../img/sig_ico_cha.png')}/>
                            <Text style={signStyles.topBottomTxtStyles}>兑换记录</Text>
                        </View>
                        <View style={{width: 0.5, height: "100%", backgroundColor: "#ebebeb"}}/>

                        <View style={signStyles.topBottomTxtConatinerStyle}>
                            <Image style={{width: 16, height: 16, resizeMode: "center"}}
                                   source={require('../../img/sig_ico_scr.png')}/>
                            <Text style={signStyles.topBottomTxtStyles}>积分记录</Text>
                        </View>
                    </View>

                </View>


                <View style={{width: "100%", height: 7, backgroundColor: "#f5f5f5"}}/>

                {/*下面的兑换专区*/}
                <View style={signStyles.bottomConatiner}>
                    {/*title*/}
                    <View style={signStyles.bottomTopContainer}>
                        <View style={{width: 3, height: "100%", backgroundColor: "#ec3627"}}/>
                        <Text style={{
                            textAlign: 'left',
                            textAlignVertical: "center",
                            fontSize: 15,
                            color: "#333333",
                            marginLeft: 12,
                        }}>积分兑换专区</Text>
                    </View>
                    {/*list*/}
                    <View style={signStyles.bootomBottomConatiner}>
                        <FlatList
                            data={[
                                {key: '优惠券'},
                                {key: '优惠券'},
                                {key: '优惠券'},
                                {key: '优惠券'},
                                {key: '优惠券'},
                                {key: '优惠券'},
                                {key: '优惠券'},
                                {key: '优惠券'},
                                {key: '优惠券'},
                                {key: '优惠券'},
                                {key: '优惠券'},
                                {key: '优惠券'},
                                {key: '优惠券'},
                                {key: '优惠券'},
                                {key: '优惠券'},
                                {key: '优惠券'},
                                {key: '优惠券'},
                                {key: '优惠券'},
                                {key: '优惠券'},
                                {key: '优惠券'},
                                {key: '优惠券'},
                                {key: '优惠券'},
                                {key: '优惠券'},
                                {key: '优惠券'},
                                {key: '优惠券'},
                                {key: '优惠券'},
                                {key: '优惠券'},
                                {key: '优惠券'},
                                {key: '优惠券'},
                                {key: '优惠券'},
                                {key: '优惠券'},
                                {key: '优惠券'},
                            ]}
                            onRefresh={this.refreshing}
                            refreshing={false}
                            numColumns={2}
                            ItemSeparatorComponent={this._separator}
                            renderItem={this._renderItem}
                        />
                    </View>
                </View>

            </View>
        );
    }
}

const signStyles = StyleSheet.create({
    topConatinerStyle: {
        flexDirection: 'column',
        height: 137.5,
        width: "100%",
        backgroundColor: "#FFFFFF",
    },
    topTopConatinerStyle: {
        flexDirection: 'row',
        height: 93.5,
        alignItems: "center",
        marginLeft: 12,
        marginRight: 12,

    },
    headImgStyle: {
        width: 49,
        height: 49,
        resizeMode: "center",
    },

    integralConatinerStyle: {
        flexDirection: 'column',
        marginLeft: 12,
        flex: 1,
    },
    integralStyle: {
        color: "#ec3627",
        fontSize: 17,
        textAlign: "left",
        textAlignVertical: "center",
    },
    integralHintStyles: {
        color: "#999999",
        fontSize: 11,
        textAlign: "left",
        textAlignVertical: "center",
        marginTop: 6,
    },
    signTxtStyle: {
        height: 24,
        width: 84,
        backgroundColor: "#ec3627",
        color: "#FFFFFF",
        fontSize: 11,
        textAlign: "center",
        textAlignVertical: "center",
        // fontWeight:"bold",
        borderRadius: 24,//设置四角弧度
    },


    topBottomConatinerStyle: {
        width: "100%",
        height: 44,
        borderTopWidth: 0.5,
        borderTopColor: "#ebebeb",
        flexDirection: 'row',
        alignItems: "center"
    },
    topBottomTxtConatinerStyle: {
        flexDirection: 'row',
        alignItems: "center",
        flex: 1,
        justifyContent: 'center',
    },
    topBottomTxtStyles: {
        color: "#666666",
        fontSize: 14,
        textAlign: "left",
        textAlignVertical: "center",
        marginLeft: 11,
    },

    bottomConatiner: {
        backgroundColor: "#FFFFFF",
        flexDirection: 'column',
        height: "100%",
    },
    bottomTopContainer: {
        flexDirection: 'row',
        height: 38,
        alignItems: "center",
        borderBottomWidth: 0.5,
        borderBottomColor: "#ebebeb",
    },
    bootomBottomConatiner: {
        width: "100%",
        backgroundColor: '#ccf8ff',

    },

    itemStyle: {
        textAlign: 'center',
        textAlignVertical: 'center',
        color: '#000000',
        fontSize: 18,
    }
});
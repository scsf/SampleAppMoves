
class User {
    /**
     * id : 92033
     * name : 2333
     * avatar_url : https://static-dev.tophold.com/uploads/user/letter_avatars/2.png?x-oss-process=style/200-200
     * im_accid : null
     * email : e86cf40f-4363-4989-bd67-48bd5a0800d1@example.com
     * authentication_token : URd85xiZkxG5-RJ6zXzE
     * exp : null
     * exp_name : null
     * summary : null
     * phone : 13303900341
     * city : null
     * user_cties : []
     * gender : false
     * order_switch : true
     * product_switch : true
     * message_switch : true
     * email_status : false
     * unread_notifications_counter : 0
     * unread_messages_counter : 0
     * identity_deadline : null
     * jifen : 50
     * followers_count : 0
     * followed_users_count : 0
     * im_token : null
     * level : 3
     * level_until : null
     * trade_amount : 50213.76
     * month_trade_amount : 50213.76
     * unused_coupons_count : 0
     * red_packet_bonus : false
     * can_change_name : false
     * continuous_days : 0
     * level_msg : false
     * level_image : https://static-dev.tophold.com/uploads/level_images/lv3.png
     * last_change_name_at : 2018-02-26T16:33:48+08:00
     */
    // 构造方法，实例化的时候将会被调用，如果不指定，那么会有一个不带参数的默认构造函数.
    constructor(id, name, avatar_url, email, phone, gender, level, jifen) {
        this.id = id;
        this.name = name;
        this.avatar_url = avatar_url;
        this.email = email;
        this.phone = phone;
        this.gender = gender;
        this.level = level;
        this.jifen = jifen;
    }

    // toString 是原型对象上的属性
    toString() {
        console.log(
            'id:' + this.id
            + ',avatar_url:' + this.avatar_url
            + ',email:' + this.email
            + ',phone:' + this.phone
            + ',gender:' + this.gender
            + ',level:' + this.level
            + ',jifen:' + this.jifen);
    }
}

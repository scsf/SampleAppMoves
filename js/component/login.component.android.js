import React, {Component} from 'react';
import {Alert, Image, StyleSheet, Text, TextInput, View} from 'react-native';


export default class LoginComponent extends Component {

    constructor(props) {
        super(props)
        this.state = ({
            user: "default",
            psw: "default",
        })
    }

    //  state = {
    //     user: "",
    //     psw: "",
    // }
    goSign() {
        const {navigate} = this.props.navigation;
        navigate('Login', {name: 'gy'})
    }

    render() {
        const {navigate} = this.props.navigation;
        return (
            <View>
                <Image
                    style={loginStyles.topImg}
                    source={require('../../img/login_bg.png')}/>

                <View style={loginStyles.coreConatinerStyle}>

                    <TextInput style={loginStyles.inputStyle}
                               placeholder="手机号/用户名/邮箱"
                               placeholderTextColor={"#b2b2b2"}
                               underlineColorAndroid={"#b2b2b2"}
                               onChangeText={(text) => {
                                   this.setState({
                                       user: text
                                   })
                               }}
                    />
                    <TextInput style={loginStyles.inputStyle}
                               placeholder="密码"
                               placeholderTextColor={"#b2b2b2"}
                               underlineColorAndroid={"#b2b2b2"}
                               secureTextEntry={true}
                               onChangeText={(text) => {
                                   this.setState({
                                       psw: text
                                   })
                               }}/>

                    <Text
                        style={loginStyles.sumStyle}
                        onPress={() => {
                            Alert.alert("提醒", "用户：" + {
                                    LoginComponent: this.state.user
                                }
                                + ",密码：" + {
                                    LoginComponent: this.state.psw
                                }
                                + "是否要登录？", [
                                    {
                                        text: "取消", onPress: () => {

                                        },
                                        style: "cancel",
                                    },
                                    {
                                        text: "确认", onPress: () => {
                                            // this.goSign();
                                            navigate('Login', {name: 'gy'})
                                        },
                                        style: "ok",
                                    },

                                ]
                            )
                        }}
                    >登录</Text>
                </View>
            </View>
        );
    }
}

const loginStyles = StyleSheet.create({
    topImg: {
        width: "100%",
        height: 184,
        resizeMode: "stretch"
    },
    coreConatinerStyle: {
        // flex: 1,
        backgroundColor: "#f5f5f5",
        flexDirection: "column",
        // justifyContent: "center",
        paddingLeft: 12,
        paddingRight: 12,
    },
    inputStyle: {},
    sumStyle: {
        height: 44,
        marginTop: 20,
        backgroundColor: "#ec3627",
        color: "#FFFFFF",
        fontSize: 17,
        textAlign: "center",
        textAlignVertical: "center",
        // fontWeight:"bold",
        borderRadius: 22,//设置四角弧度
    }
});
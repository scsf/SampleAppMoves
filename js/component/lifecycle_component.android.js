import React, {Component} from 'react';
import {
    Text,
    View
} from 'react-native';

export default class lifecycle_component extends Component {

    /**
     * 构造方法，在最开始被调用
     */
    constructor() {
        super();
        console.log("-----constructor-----");
        this.state = {
            count: 0
        }
    }

    /**
     *
     * js将要被装载
     *
     */
    componentWillMount() {
        console.log("-----componentWillMount-----");

    }

    /**
     * 渲染
     * @returns {*}
     */
    render() {
        console.log("-----render-----");
        return (
            <View>
                <Text style={{fontSize: 20, backgroundColor: 'red'}}
                      onPress={() => {
                          this.setState({
                              count: this.state.count + 1,
                          })
                      }}>点击打他一下</Text>
                <Text>被打了{this.state.count}下</Text>
            </View>
        );
    }

    /**
     * 页面在第一次被渲染后调用，这个时候可以进行各种在页面初始化之后进行的各种操作
     */
    componentDidMount() {
        console.log("-----componentDidMount-----");
    }

    /**
     * 这个什么时候调用？？没看到。
     */
    componentWillReceiveProps(nextProps) {
        console.log("-----componentWillReceiveProps-----" + nextProps);
    }

    /**
     * 当组件更新时候会被调用
     * @param nextProps
     * @param nextState
     * @returns {boolean} 是否更新render()方法
     */
    shouldComponentUpdate(nextProps, nextState) {
        console.log("-----shouldComponentUpdate-----" + nextProps + "," + nextState);
        return true;
    }

    /**
     * 组件被移除，重新被加载会重新走生命周期
     */
    componentWillUnmount() {
        console.log("-----componentWillUnmount-----");

    }
}
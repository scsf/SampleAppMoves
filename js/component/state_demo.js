import React, {Component} from 'react';
import {
    Text,
    View,
    Image,
    Button
} from 'react-native';


export default class props_demo extends Component {

    state = {
        size: 80,
        name: 'gy'
    }


    // constructor() {
    //     super();
    //     this.state = ({
    //         size: 80,
    //         name: 'gy'
    //     })
    // }

    render() {
        return (

            <View>
                <Image style={{width: this.state.size, height: this.state.size}}
                       source={require('../../img/img.png')}/>

                <View>
                    <Button title="变小" onPress={() => {
                        this.setState({size: this.state.size - 10})
                    }}/>
                    <Button title="变大" style={{marginTop: 135}} onPress={() => {
                        this.setState({size: this.state.size + 10})
                    }}/>
                </View>
            </View>

        );
    }
}




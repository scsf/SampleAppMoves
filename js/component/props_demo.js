import React, {
    Component,
    // PropTypes
} from 'react';
import {
    Text,
    View,
} from 'react-native';


export default class props_demo extends Component {

    /**
     * 默认的属性值
     * @type {{name: string}}
     */
    static defaultProps = {
        name: "小红",
        age: 16,
    }

    // static propTypes = {
    //     name: React.PropTypes.string.isRequired,
    //     age: React.PropTypes.int.isRequired,
    // }


    render() {
        return (
            <View>
                <Text>
                    Hello,{this.props.name},{this.props.age}
                </Text>
            </View>
        );
    }
}






import React, {Component} from 'react';
import {
    Text,
    View,
    Image,
    Button,
    StyleSheet
} from 'react-native';


export default class props_demo extends Component {

    state = {
        size: 80,
        name: 'gy'
    }


    // constructor() {
    //     super();
    //     this.state = ({
    //         size: 80,
    //         name: 'gy'
    //     })
    // }

    render() {
        return (

            <View>
                <Image style={styles.img}
                       source={require('../../img/img.png')}/>

                <View>
                    <Button title="变小" onPress={() => {
                        this.setState({size: this.state.size - 10})
                    }}/>
                    <Button title="变大" style={{marginTop: 135}} onPress={() => {
                        this.setState({size: this.state.size + 10})
                    }}/>

                    <Image style={{width: 200, height: 100, alignSelf: 'center', marginTop: 100}}
                           source={{uri: 'https://pic2.zhimg.com/v2-f675f09d3e54d75db09f206065581f34_b.jpg'}}/>
                    <Image
                        style={{width: 200, height: 100, alignSelf: 'center', marginTop: 100,resizeMode:'center',backgroundColor:'#0f0'}}
                        source={{uri: 'tiao'}}/>
                </View>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    img: {
        borderWidth: 3,
        resizeMode: 'cover',
        width: 100,
        height: 50,
        alignSelf: 'center'
    }
});



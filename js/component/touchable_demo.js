import React, {Component} from 'react';
import {
    Text,
    View,
    Image,
    Button,
    StyleSheet,
    TouchableWithoutFeedback,
    TouchableHighlight,
    TouchableNativeFeedback,
    Alert
} from 'react-native';


export default class layout_demo extends Component {

    state = {
        count: 0,
        txt: "登录",
    }

    render() {
        return (
            <View style={styles.containerStyle}>

                <TouchableWithoutFeedback
                    onPress={() => {
                        this.setState({count: this.state.count + 1})
                    }}

                    onLongPress={() => {
                        Alert.alert("提醒", "确认删除吗？", [
                            {
                                text: "取消", onPress: () => {
                                }, style: "cancel"
                            },
                            {
                                text: "确认", onPress: () => {
                                }, style: "ok"
                            },
                        ])
                    }}

                    onPressIn={() => {

                    }}

                    onPressOut={() => {

                    }}
                >

                    <Text style={styles.item}>{this.state.count}</Text>
                </TouchableWithoutFeedback>


                <TouchableHighlight
                    //不透明度
                    activeOpacity={0.8}
                    underlayColor={'#f0f'}

                    onPress={() => {

                    }}
                >
                    <Text style={styles.item}>B</Text>
                </TouchableHighlight>


                <TouchableNativeFeedback
                    background={TouchableNativeFeedback.SelectableBackground()}>

                    {/*为什么修改宽度没有效果？？？*/}
                    <Button style={styles.item} title={this.state.count.toString()}
                            onPress={() => {
                                this.setState({count: this.state.count + 1})
                            }}/>

                </TouchableNativeFeedback>


            </View>

        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        //排列方式
        //flexDirection: 'column',//默认方式
        flexDirection: 'row',
        //flexDirection: 'column-reverse',
        //flexDirection: 'row-reverse',


        //是否允许换行
        // flexWrap: 'nowrap',//默认不允许换行
        //flexWrap: 'wrap',

        //子控件排列方式,横轴
        // justifyContent:'flex-start',//默认
        // justifyContent:'flex-end',//
        justifyContent: 'center',//
        justifyContent: 'space-between',//在每行上均匀分配弹性元素。相邻元素间距离相同。每行第一个元素与行首对齐，每行最后一个元素与行尾对齐。
        //justifyContent: 'space-around',//在每行上均匀分配弹性元素。相邻元素间距离相同。每行第一个元素到行首的距离和每行最后一个元素到行尾的距离将会是相邻元素之间距离的一半。


        //alignItems，竖轴
        //alignItems: 'flex-start',
        //alignItems:'flex-end',
        alignItems: 'center',
        //alignItems:'stretch',//拉伸
    },

    item: {
        width: 40,
        height: 40,
        backgroundColor: '#7effa3',

        //alignSelf:属性以属性定义了flex容器内被选中项目的对齐方式。注意：alignSelf 属性可重写灵活容器的 alignItems 属性。具体属性和alignItems对应。
        // alignSelf:'auto',//default
        //alignSelf: 'flex-end',


        //flex: 1,

        //其它属性
        borderWidth: 2,
        borderColor: '#ff3c1d',
        padding: 0,
        //top:50,

        //文本专有属性
        textAlign: 'center'

    },
    button: {
        //width: 80,
        // height: 35,
        // borderWidth: 6,
        borderColor: "#ff041c",
        flex: 1,
    }
});




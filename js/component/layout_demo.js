import React, {Component} from 'react';
import {
    Text,
    View,
    Image,
    Button, StyleSheet
} from 'react-native';


export default class layout_demo extends Component {

    render() {
        return (
            <View style={styles.containerStyle}>
                <Text style={styles.item}>A</Text>
                <Text style={styles.item}>B</Text>
                <Text style={styles.item}>C</Text>
                {/*<Text style={styles.item}>D</Text>*/}
                {/*<Text style={styles.item}>E</Text>*/}
                {/*<Text style={styles.item}>F</Text>*/}
            </View>

        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        //排列方式
        //flexDirection: 'column',//默认方式
        flexDirection: 'row',
        //flexDirection: 'column-reverse',
        //flexDirection: 'row-reverse',


        //是否允许换行
        // flexWrap: 'nowrap',//默认不允许换行
        flexWrap: 'wrap',

        //子控件排列方式,横轴
        // justifyContent:'flex-start',//默认
        // justifyContent:'flex-end',//
        //justifyContent: 'center',//
        justifyContent: 'space-between',//在每行上均匀分配弹性元素。相邻元素间距离相同。每行第一个元素与行首对齐，每行最后一个元素与行尾对齐。
        //justifyContent: 'space-around',//在每行上均匀分配弹性元素。相邻元素间距离相同。每行第一个元素到行首的距离和每行最后一个元素到行尾的距离将会是相邻元素之间距离的一半。


        //alignItems，竖轴
        //alignItems: 'flex-start',
        //alignItems:'flex-end',
        alignItems: 'center',
        //alignItems:'stretch',//拉伸
    },

    item: {
        width: 100,
        //height: 40,
        backgroundColor: '#7effa3',

        //alignSelf:属性以属性定义了flex容器内被选中项目的对齐方式。注意：alignSelf 属性可重写灵活容器的 alignItems 属性。具体属性和alignItems对应。
        // alignSelf:'auto',//default
        //alignSelf: 'flex-end',


        //flex: 1,

        //其它属性
        borderBottomWidth:10,
        borderBottomColor:'#352bff',
        padding:10,
        //top:50,

        //文本专有属性
        textAlign:'center'

    }
});



